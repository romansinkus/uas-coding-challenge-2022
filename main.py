import re
import math

print("Line Intersection Challenge: ")
coord1 = input("First coordinate of the line in the form (x,y): ")
coord2 = input("Second coordinate of the line in the form (x,y): ")
coordCircle = input("Center of the circle in the form (x,y): ")
radius = int(input("Radius of the circle: "))

newCoord1 = re.sub(r"[\(\)]",'',coord1)
newCoord2 = re.sub(r"[\(\)]",'',coord2)
newCoordCircle = re.sub(r"[\(\)]",'',coordCircle)

x1, y1 = newCoord1.split(",")
x2, y2 = newCoord2.split(",")
circleX, circleY = newCoordCircle.split(",")

x1 = int(x1)
y1 = int(y1)
x2 = int(x2)
y2 = int(y2)
circleX = int(circleX)
circleY = int(circleY)
m = 0
b = 0

if y1 == y2:
      m = 0
else:
      m = ((y2-y1) / (x2-x1))

b = y1 - (m * x1)

t1 = (1 + m**2)
t2 = ((-2 * circleX) + (2 * b * m) - (2 * circleY * m))
t3 = (b**2 - (2 * circleY * b) + circleX**2 + circleY**2 - radius**2)

disc = t2**2 - (4 * t1 * t3)
if disc > 0:
      print("Two intersections")
      xVal1 = (-t2-math.sqrt(disc))/(2*t1)
      xVal2 = (-t2+math.sqrt(disc))/(2*t1)
      yVal1 = (m * xVal1) + b
      yVal2 = (m * xVal2) + b
      xVal1 = f'{xVal1:.2f}'
      xVal2 = f'{xVal2:.2f}'
      yVal1 = f'{yVal1:.2f}'
      yVal2 = f'{yVal2:.2f}'
      print("Intersecting Point #1: (" + str(xVal1) + "," + str(yVal1) + ")")
      print("Intersecting Point #2: (" + str(xVal2) + "," + str(yVal2) + ")")

elif disc == 0:
      print("One Intersection")
      xVal1 = (-t2 / (2 * t1))
      yval1 = yVal1 = (m * xVal1) + b
      xVal1 = f'{xVal1:.2f}'
      yVal1 = f'{yVal1:.2f}'
      print("Intersecting Point: (" + str(xVal1) + "," + str(yVal1) + ")")
else:
      print("No Intersections")
